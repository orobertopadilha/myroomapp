import React from 'react';
import LoginScreen from './app/screens/login';
import { AuthProvider } from './app/store/context/auth';

const App = () => { 
    return (
        <AuthProvider>
            <LoginScreen />
        </AuthProvider>
    )
}

export default App;
