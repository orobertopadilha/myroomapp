export interface ExampleState {
    count: number
}

export enum ExampleAction {
    INCREMENT,
    DECREMENT
}