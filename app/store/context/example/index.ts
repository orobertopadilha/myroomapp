import { createContext } from 'react'

const names = [ 'Joao', 'Pedro', 'Paulo', 'Maria', 'Jose' ]

export const ExampleContext = createContext(names)

