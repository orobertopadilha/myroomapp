import axios from 'axios'

const createApi = () => { 
    let http = axios.create({ 
        baseURL: 'http://192.168.2.27:9090/'
    })
    return http
}

const api = createApi()

export default api